﻿using HtmlAgilityPack;
using SpiderSites.Model;
using System.Collections.Generic;

namespace SpiderSites.Core
{
    public interface IParser<T> where T : class
    {
        ProviderStateModel State { get; set; }
        List<UrlModel> Urls { get; set; }

        T Parse(HtmlDocument document);

        void AddUrls(T urls);
    }
}