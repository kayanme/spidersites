﻿using HtmlAgilityPack;
using SpiderSites.Model;
using System;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

namespace SpiderSites.Core
{
    public class ParserWorker<T> where T : class
    {
        #region Fields & property

        private IParser<T> _parser;
        private IParserSetting _parserSettings;

        private HtmlLoader _htmlLoader;

        public IParser<T> Parser
        {
            get { return _parser; }
            set { _parser = value; }
        }

        public IParserSetting ParserSettings
        {
            get { return _parserSettings; }
            set { _parserSettings = value; }
        }

        public bool IsActive { get; set; }

        private bool _isActive
        {
            get { return IsActive; }
        }

        #endregion Fields & property

        public event Action OnNewData;

        public event Action OnCompleted;

        public event Action OnError;

        #region Ctor

        public ParserWorker(IParser<T> parser)
        {
            _parser = parser;
        }

        public ParserWorker(IParser<T> parser, IParserSetting parserSettings) : this(parser)
        {
            _parserSettings = parserSettings;
            _htmlLoader = new HtmlLoader(parserSettings);
        }

        #endregion Ctor

        public void Start()
        {
            IsActive = true;
            Worker();
        }

        public void Abort()
        {
            IsActive = false;
        }

        private async Task Worker()
        {
            try
            {
                while (_isActive)
                {
                    #region GetItem

                    var item = _parser.Urls.FirstOrDefault(s => s.State == UrlModel.UrlStateType.None);
                    if (item == null)
                    {
                        if (_parser.Urls.All(s => s.State == UrlModel.UrlStateType.Complete || s.State == UrlModel.UrlStateType.OtherLink))
                            break;

                        continue;
                    }

                    item.State = UrlModel.UrlStateType.Parse;

                    #endregion GetItem

                    #region Check url

                    if (Regex.IsMatch(item.Url, "^(http|https).{1,}", RegexOptions.IgnoreCase))
                    {
                        if (Regex.IsMatch(item.Url, $"^(http|https):\\/\\/{_parserSettings.BaseUrl}"))
                            item.Url = Regex.Replace(item.Url, $"^(http|https):\\/\\/{_parserSettings.BaseUrl}", string.Empty);
                        else
                        {
                            item.State = UrlModel.UrlStateType.OtherLink;
                            continue;
                        }
                    }

                    #endregion Check url

                    var htmlSourceRaw = await _htmlLoader.GetSourcePage(item.Url);
                    if (!htmlSourceRaw.IsOk)
                        throw new Exception(htmlSourceRaw.Error);

                    var document = new HtmlDocument();
                    document.LoadHtml(htmlSourceRaw.Data);
                    var urls = _parser.Parse(document);
                    _parser.AddUrls(urls);

                    item.State = UrlModel.UrlStateType.Complete;
                    OnNewData?.Invoke();

                    Thread.Sleep(1000);
                }
            }
            catch (Exception e)
            {
                _parser.State.Status = $"Ошибка: {e.Message}";
                OnError?.Invoke();
                return;
            }

            OnCompleted?.Invoke();
        }
    }
}