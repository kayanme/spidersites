﻿namespace SpiderSites.Core
{
    public interface IParserSetting
    {
        string BaseUrl { get; set; }
        bool UseHttps { get; set; }
    }
}