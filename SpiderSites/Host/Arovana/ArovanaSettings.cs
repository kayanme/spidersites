﻿using SpiderSites.Core;

namespace SpiderSites.Host.Arovana
{
    public class ArovanaSettings : IParserSetting
    {
        public bool UseHttps { get; set; } = true;
        public string BaseUrl { get; set; } = "xn--80aaag0ddehdg9b.xn--p1ai1";
    }
}