﻿using System.Collections.Generic;
using System.Linq;
using HtmlAgilityPack;
using SpiderSites.Core;
using SpiderSites.Model;

namespace SpiderSites.Host.Arovana
{
    internal class ArovanaParser : IParser<string[]>
    {
        public ProviderStateModel State { get; set; }
        public List<UrlModel> Urls { get; set; }

        public ArovanaParser()
        {
            Urls = new List<UrlModel> { new UrlModel { Url = "/" } };
            State = new ProviderStateModel { Name = "Арована", Status = "Инициализация", Link = "1" };
        }

        public string[] Parse(HtmlDocument document)
        {
            var tagLinkItems = document.DocumentNode.Descendants("a").ToArray();

            var items = new string[tagLinkItems.Length];
            for (var i = 0; i < tagLinkItems.Length; i++)
                items[i] = tagLinkItems[i].Attributes["href"].Value;

            return items;
        }

        public void AddUrls(string[] urls)
        {
            Urls = Urls.Union(urls.Select(s => new UrlModel { Url = s })).ToList();
            State.Link = $"{Urls.Count:N0} шт";
        }
    }
}