﻿using SpiderSites.Core;
using SpiderSites.Host.Arovana;
using SpiderSites.Model;
using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace SpiderSites
{
    public partial class MainForm : Form
    {
        #region Fields & property

        private readonly ParserWorker<string[]> _parser;

        #endregion Fields & property

        #region Ctor

        public MainForm()
        {
            InitializeComponent();

            #region Init parser

            _parser = new ParserWorker<string[]>(new ArovanaParser(), new ArovanaSettings());
            _parser.OnNewData += ParserOnOnNewData;
            _parser.OnCompleted += ParserOnOnCompleted;
            _parser.OnError += ParserOnOnError;

            #endregion Init parser

            var states = new List<ProviderStateModel> { _parser.Parser.State };
            DgvProviderState.DataSource = states;
        }

        #endregion Ctor

        private void BtnStart_Click(object sender, EventArgs e)
        {
            _parser.Start();

            btnStart.Enabled = false;
            btnStop.Enabled = true;
        }

        private void BtnStop_Click(object sender, EventArgs e)
        {
            _parser.Abort();
        }

        #region Event

        private void ParserOnOnNewData()
        {
            DgvProviderState.Refresh();
        }

        private void ParserOnOnCompleted()
        {
            btnStart.Enabled = true;
            btnStop.Enabled = false;

            DgvProviderState.Refresh();
            MessageBox.Show("Сбор данных завершен");
        }

        private void ParserOnOnError()
        {
            btnStart.Enabled = true;
            btnStop.Enabled = false;

            DgvProviderState.Refresh();
            MessageBox.Show("Ошибка при сборе данных");
        }

        #endregion Event
    }
}