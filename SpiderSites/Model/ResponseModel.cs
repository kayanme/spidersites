﻿using System.Net;

namespace SpiderSites.Model
{
    /// <summary>
    /// Модель HTTP ответа
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class ResponseModel<T>
    {
        /// <summary>
        /// Данные
        /// </summary>
        public T Data { get; set; }

        /// <summary>
        /// Статус ошибки
        /// </summary>
        public HttpStatusCode Code { get; set; }

        /// <summary>
        /// Ошибка
        /// </summary>
        public string Error { get; set; }

        /// <summary>
        /// Результат выполнения
        /// </summary>
        public bool IsOk { get; set; }
    }
}