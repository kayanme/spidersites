﻿using System.ComponentModel;

namespace SpiderSites.Model
{
    /// <summary>
    /// Модель состояния провайдера данных
    /// </summary>
    public class ProviderStateModel
    {
        [DisplayName("Название")]
        public string Name { get; set; }

        [DisplayName("Кол-во ссылок")]
        public string Link { get; set; }

        [DisplayName("Состояние")]
        public string Status { get; set; }
    }
}