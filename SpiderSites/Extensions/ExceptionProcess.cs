﻿using System;

namespace SpiderSites.Extensions
{
    /// <summary>
    /// Контроль процесса
    /// </summary>
    public class ExceptionProcess : Exception
    {
        /// <summary>
        /// Сообщение с ошибкой
        /// </summary>
        public new readonly string Message;

        /// <summary>
        /// Тип ошибки
        /// </summary>
        public readonly ErrorType Type;

        public ExceptionProcess(string message, ErrorType type = ErrorType.Error)
        {
            Message = message;
            Type = type;
        }


        /// <summary>
        /// Типы ошибок
        /// </summary>
        public enum ErrorType
        {
            Error,
            NetworkConnection,
        }
    }
}